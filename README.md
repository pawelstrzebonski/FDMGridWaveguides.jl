# FDMGridWaveguides

## About

`FDMGridWaveguides.jl` is a set of utilities to assist in defining and
generating waveguide structures for the
[`FDMModes`](https://gitlab.com/pawelstrzebonski/FDMModes.jl)
mode solver package.

## Installation

This package is not in the official Julia package repository, so to
install, run in Julia:

```Julia
]add https://gitlab.com/pawelstrzebonski/FDMGridWaveguides.jl
```

## Documentation

Package documentation and usage examples can be found at the
[Gitlab Pages for FDMGridWaveguides.jl](https://pawelstrzebonski.gitlab.io/FDMGridWaveguides.jl/).
