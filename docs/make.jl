using Documenter
import FDMGridWaveguides

makedocs(
    sitename = "FDMGridWaveguides.jl",
    repo = Documenter.Remotes.GitLab("pawelstrzebonski", "FDMGridWaveguides.jl"),
    pages = [
        "Home" => "index.md",
        "Examples and Usage" => ["Basic Usage" => "example.md"],
        "References" => "references.md",
        "Contributing" => "contributing.md",
        "src/" => ["waveguide_gen.jl" => "waveguide_gen.md"],
        "test/" => ["waveguide_gen.jl" => "waveguide_gen_test.md"],
    ],
)
