# Contributing and Development

Contributions to this package are welcomed. Please submit issues/pull-requests
on the GitLab project.

## TODOs

The following are some areas for development and improvement:

* More, better unit tests and documentation
* Implement boundary conditions
* Add utilities for generating structures useful for modeling other waveguide structures (perhaps optical fibers, or VCSELs)
* More advanced sample spacing algorithms (implementing simple uniform sampling, perhaps graded sampling to avoid large changes in sample spacing)

## Guidance

When adding functionality or making modifications, please keep in mind
the following:

* We aim to support both 1D and 2D structures
* We aim to produce output appropriate for use with the [`FDMModes`](https://gitlab.com/pawelstrzebonski/FDMModes.jl) package
* We aim to minimize the amount of code and function repetition when implementing the above features
