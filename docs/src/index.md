# FDMGridWaveguides.jl

The `FDMGridWaveguides` package provides some functions to help generate
waveguide index structure arrays, as appropriate for use in the
[`FDMModes`](https://gitlab.com/pawelstrzebonski/FDMModes.jl)
mode solving package.

## Installation

This package is not in the official Julia package repository, so to
install, run in Julia:

```Julia
]add https://gitlab.com/pawelstrzebonski/FDMGridWaveguides.jl
```

## Features

* 1D "banded" waveguide structures (waveguide which can be represented as a stack of constant index regions)
* 2D "banded" waveguide structures (waveguide which can be represented as a stack of 1D "banded" waveguides)
* Non-uniform grid sampling
