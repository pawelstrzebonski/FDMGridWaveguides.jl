#=
TODO: make constant dx versions of these
TODO: instead of appending to create matrices, use matrix multiplication
	(i.e. bandchooser*indexslices)
TODO: More functionality? (symmetry, boundary conditions, etc?)
=#

"""
    mkbandedwvg(nclad, ncore, npert, aclad, acore, apert, wclad, wbands)->(w, n, a)

Construct the (lateral) width, index, and active region factor arrays,
as required for the widths2index function given array of widths for the
alternating core layers of `ncore` and `npert`.
"""
function mkbandedwvg(
    nclad::Number,
    ncore::Number,
    npert::Number,
    aclad::Number,
    acore::Number,
    apert::Number,
    wclad::Number,
    wbands::AbstractVector,
)
    w = [wclad; wbands; wclad]
    n = [nclad; (x -> isodd(x) ? ncore : npert).(1:length(wbands)); nclad]
    a = [aclad; (x -> isodd(x) ? acore : apert).(1:length(wbands)); aclad]
    w, n, a
end
function mkbandedwvg(
    nclad::AbstractVector,
    ncore::AbstractVector,
    npert::AbstractVector,
    aclad::AbstractVector,
    acore::AbstractVector,
    apert::AbstractVector,
    wclad::Number,
    wbands::AbstractVector,
)
    w = [wclad; wbands; wclad]
    n = hcat(nclad, (x -> isodd(x) ? ncore : npert).(1:length(wbands))..., nclad)
    a = hcat(aclad, (x -> isodd(x) ? acore : apert).(1:length(wbands))..., aclad)
    w, n, a
end

"""
    trunc_arr(w, l)

Truncate array `w` so that the sum of the array will be `l`.
"""
function trunc_arr(w0::AbstractVector, l::Number)
    @assert sum(w0) >= l
    w = w0[cumsum(w0).<l]
    [w; l - sum(w)]
end

"""
    selectormatrix(w)

Given array `w` of coefficients, create a matrix that when multiplied
by a vector/matrix will repeat the corresponding components of a
vector/matrix `w` many times.
"""
function selectormatrix(w::AbstractVector)
    a = zeros(sum(w), length(w))
    ww = [0; cumsum(w)]
    [a[(ww[i]+1):ww[i+1], i] .= 1 for i = 1:length(w)]
    a
end

"""
    widths2index(w, n, a, lambda0; dxmax=0)->(index, dx, active)

Give the discrete index structure, sample spacing, and active region mask
given arrays of width `w`, index `n`, and active mask `a`. Sampling is
not necessarily unform and the maximal spacing will be a 10th of a in-
material wavelength if not specified (ie `dxmax=0`).
"""
function widths2index(
    w::AbstractVector,
    n::AbstractVector,
    a::AbstractVector,
    lambda0::Number;
    dxmax::Number = 0,
)
    @assert length(w) == length(n) == length(a)
    if iszero(dxmax)
        dxmax = lambda0 / (10 * maximum(real.(n)))
    end
    x = round.(Int, real.(w / dxmax), RoundUp)
    sm = selectormatrix(x)
    index = sm * n
    dx = sm * (w ./ x)
    active = sm * a
    index, dx, active
end

"""
    widths2index(w_lat, w_epi, n_epi, a_epi, lambda0; dxmax=0)->(index, dx_epi, dx_lat, active)

Give the discrete index structure, sample spacing, and active region mask
given arrays of epitaxial layer widths `w_epi`, epitaxial layer indices
`n_epi`, and epitaxial layer active masks `a_epi`, and the widths of
lateral slices `w_lat`. These are two dimensional arrays, with sizes
(# of epitaxial layers)x(# of lateral slices). Sampling is
not necessarily unform and the maximal spacing will be a 10th of a in-
material wavelength if not specified (ie `dxmax=0`).
"""
function widths2index(
    w_lat::AbstractVector,
    w_epi::AbstractVector,
    n_epi::AbstractMatrix,
    a_epi::AbstractMatrix,
    lambda0::Number;
    dxmax::Number = 0,
)
    @assert size(n_epi) == size(a_epi)
    @assert length(w_lat) == size(n_epi, 2)
    @assert length(w_epi) == size(n_epi, 1)
    if iszero(dxmax)
        dxmax = lambda0 / (10 * maximum(real.(n_epi)))
    end
    epi_slices = [
        widths2index(w_epi, n_epi[:, i], a_epi[:, i], lambda0, dxmax = dxmax)
        for i = 1:length(w_lat)
    ]
    dx_epi = epi_slices[1][2]
    n = hcat((x -> x[1]).(epi_slices)...)
    a = hcat((x -> x[3]).(epi_slices)...)
    x_lat = round.(Int, real.(w_lat / dxmax), RoundUp)
    sm = selectormatrix(x_lat)
    index = n * sm'
    dx_lat = sm * (w_lat ./ x_lat)
    active = a * sm'
    index, dx_epi, dx_lat, active
end
