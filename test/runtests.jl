import FDMGridWaveguides
import Test: @test_broken, @test, @test_throws

tests = ["waveguide_gen"]

approxeq(a, b) = all(isapprox.(a, b, rtol = 1e-4))

aeq(a, b) = all(isapprox.(a, b, rtol = 1e-4))

for t in tests
    @info "Running " * t * ".jl"
    include("$(t).jl")
    @info "Finished " * t * ".jl"
end
